#!/usr/bin/env python

import requests
from bs4 import BeautifulSoup
import sys



class ParserHTML():
    def __init__(self):

        # argument1 : URL
        self.url = sys.argv[1]
        # argument2 : class div content posts
        self.div1 = sys.argv[2]
        # argument3 : candidad de post a extrer
        self.limit = sys.argv[3]
        self.page = requests.get(self.url)
        self.parser = BeautifulSoup(self.page.content,"lxml")
        

    def parserDiv(self):
        return self.parser.body.find_all('div',attrs={'class':'post-content'})
        



if __name__ == '__main__':
    
    parser = ParserHTML()
    print "\n"*3
    print 'Extracting %s first News'%(parser.limit)
    print "\n"*3
    print " ------------------------------------------------- "
    parseddivs = parser.parserDiv()
    posts = []
    for i,element in enumerate(parseddivs):
    	try:
    	    if i<int(parser.limit) : posts.append(element)
    	except Exception, e:
    		print str(e)

 
    for post in posts:
    	print post
    	print " ------------------------------------------------- "
    