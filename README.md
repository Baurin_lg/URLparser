Para ejecutar en terminal: 
 $./extractnews.py <url> <div-content> <limit>
 
donde: 
 url : La url de la que quieres extraer contenido
 div-content : el nombre de la clase del divisor que contiene el contenido deseado
 limit : el numero de contenidos que se desea extraer.
 
